/* Customizable properties */
let fps = 1/25; /* Video fps */
let distanceFpsDivide = 25; /* Speed video playing when dragging */
let inertiaBrake = 50; /* Inertia max time each frame */
let inertiaDivide = 150; /* 0 represent infinite inertia, more the value is high, more the inertia is low */
let timeEachHandFrame = 20;
/* End customizable properties */

let dragging = false;
let oldCursorPosition, cursorPosition;
let positive;
let globalDistance = 0;
let animation, inertiaAnimation; /* RequestAnimationFrame variables */
let startDrag, busy = false;
let draggingEndTime, inertiaMsWatchDog = 500;

let cursorY, cursorX, startClick, cursorAnimationFrame;

let alreadyReachedEnd = false;

let inertiaDistanceToTravel, inertiaDistanceTravel;

let macOsPlatform = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'];

document.addEventListener("DOMContentLoaded", () => {

    if (navigator.userAgent.indexOf("Firefox") !== -1 && macOsPlatform.find(os => os === navigator.platform) !== undefined) {
        inertiaDivide = 225;
    }

    let video = document.getElementById("video");
    let container = document.getElementById("videoContainer");
    let videoCursor = document.getElementById("video-cursor");

    let progressBar = document.getElementById("progressBar");

    function drag() {
        if (!busy && dragging && oldCursorPosition !== cursorPosition) {
            let distance = Math.abs(oldCursorPosition - cursorPosition);
            if (!isNaN(distance) && oldCursorPosition !== null) {
                globalDistance += distance;
                let targetTime = fps * (distance / distanceFpsDivide);
                positive = oldCursorPosition < cursorPosition ? -1 : 1;
                if (video.currentTime <= 0 && alreadyReachedEnd) {
                    video.currentTime = video.duration;
                } else if (video.currentTime >= video.duration) {
                    video.currentTime = 0;
                    if (!alreadyReachedEnd)
                        alreadyReachedEnd = true;
                }
                video.currentTime = video.currentTime + targetTime * positive;
                busy = true;
            }
            oldCursorPosition = cursorPosition;
        }
        animation = requestAnimationFrame(drag);
    }
    animation = requestAnimationFrame(drag);

    /* Dragging controller */
    container.addEventListener("mousedown", draggingStart);
    container.addEventListener("mouseup", draggingEnd);
    container.addEventListener("mouseleave", draggingEnd);
    container.addEventListener("mousemove", updateCursorPosition);
    video.addEventListener("seeked", function() {
        busy = false;
    });

    container.addEventListener("mouseenter", function (event) {
        videoCursor.classList.remove("invisible");
        requestAnimationFrame(refreshHandPosition);
    });
    container.addEventListener("mouseleave", function () {
        videoCursor.classList.add("invisible");
    });

    container.addEventListener("mousemove", function(event) {
        if (event.clientX-container.offsetLeft > 0 && event.clientX-container.offsetLeft < container.offsetWidth) {
            cursorX = videoCursor.style.left = ((event.clientX-container.offsetLeft) - videoCursor.offsetWidth/2) + "px";
        }
        if (event.clientY-container.offsetTop > 0 && event.clientY-container.offsetTop < container.offsetHeight) {
            cursorY = videoCursor.style.top = ((event.clientY-container.offsetTop) - videoCursor.offsetHeight/2) + "px";
        }
    });

    function refreshHandPosition() {
        videoCursor.style.top = cursorY;
        videoCursor.style.left = cursorX;
        requestAnimationFrame(refreshHandPosition);
    }

    /* Update progress bar */
    video.addEventListener("timeupdate", function() {
        progressBar.style.width = video.currentTime * 100 / video.duration + "%";
    });

    function updateCursorPosition(event) {
        cursorPosition = event.clientX;
    }

    function cursorAnimation(time) {
        let timePassed = time - startClick;
        if (timePassed > timeEachHandFrame * 3) {
            videoCursor.style.backgroundImage = "url('assets/4.svg')";
            cancelAnimationFrame(cursorAnimationFrame);
            return;
        }
        else if (timePassed > timeEachHandFrame * 2) {
            videoCursor.style.backgroundImage = "url('assets/3.svg')";
        }
        else if (timePassed > timeEachHandFrame) {
            videoCursor.style.backgroundImage = "url('assets/2.svg')";
        }
        cursorAnimationFrame = requestAnimationFrame(cursorAnimation);
    }

    function draggingStart(event) {
        startClick = performance.now();
        cursorAnimationFrame = requestAnimationFrame(cursorAnimation);
        cancelAnimationFrame(inertiaAnimation);
        dragging = true;
        startDrag = performance.now();
        cursorPosition = event.clientX;
        animation = requestAnimationFrame(drag);
    }

    function draggingEnd() {
        videoCursor.style.backgroundImage = "url('assets/1.svg')";
        if (dragging) {
            let totalTimeUserDragging = (performance.now()-startDrag)/1000;
            let inertiaFrameToTravel = globalDistance/totalTimeUserDragging * fps / inertiaDivide;
            let targetInertiaFrame = video.currentTime + (inertiaFrameToTravel*positive);

            inertiaDistanceToTravel = Math.abs(targetInertiaFrame - video.currentTime);
            inertiaDistanceTravel = 0;

            inertiaAnimation = requestAnimationFrame(inertiaDragging);
            dragging = false;
            draggingEndTime = performance.now();
        }
        oldCursorPosition = null;
        cursorPosition = null;
        globalDistance = 0;
    }

    function inertiaDragging(time) {
        if (busy) {
            if ((time - draggingEndTime) > inertiaMsWatchDog)
                return false;
            else
                inertiaAnimation = requestAnimationFrame(inertiaDragging);
        }
        else {
            let progress = inertiaDistanceTravel / inertiaDistanceToTravel;

            if (progress < 1) {
                if (video.currentTime === 0 && alreadyReachedEnd)
                    video.currentTime = video.duration;
                else if (video.currentTime === video.duration) {
                    video.currentTime = 0;
                    alreadyReachedEnd = true;
                }

                inertiaDistanceTravel += fps;
                busy = true;
                video.currentTime += (fps*positive);
                setTimeout(function() {
                    inertiaAnimation = requestAnimationFrame(inertiaDragging);
                }, progress*inertiaBrake);
            }
        }
    }
});